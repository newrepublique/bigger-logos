var targets = '[class*="logo"], [id*="logo"], [id*="Logo"], [class*="Logo"], .title.ir, #reactiveBrand, .Logo img, #logo img, #header-img, .brand.absolute, .home-link'

function setOn(){
  $(targets).each(function() {
    $(this).css({
      'transform' : 'scale(2.5)',
      });
  });
};

function setOff(){
  $(targets).each(function() {
    $(this).css({
      'transform' : 'scale(1)',
      });
  });
};
